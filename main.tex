\documentclass{article}

\usepackage[
    a4paper,
    margin=1in,
    headsep=10pt,
]{geometry}
\usepackage{apacite}
\usepackage{cleveref}
\usepackage{enumitem}
\usepackage{fancyhdr}
\usepackage{listings}
\usepackage{tabularx}
\usepackage{xcolor}

\setlist{noitemsep}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[C]{%
    \small\sffamily
    \fullname\quad
    \email\quad
    \phone}
\fancyfoot[C]{\thepage}

\lstset{%
    breaklines=true,
    postbreak=\raisebox{0ex}[0ex][0ex]{%
        \ensuremath{\color{red}\hookrightarrow\space}}
}

\newcommand{\fullname}{Koen\ Dercksen (4215966)}
\newcommand{\email}{mail@koendercksen.com}
\newcommand{\phone}{06--41327492}
\newcommand{\doctitle}{Effect Of Random Forest Pruning On Performance And
Scalability}
\newcommand{\supervisors}{Arjen de Vries (FNWI) \& Johan Kwisthout (FSW)}

\renewcommand{\arraystretch}{1.5}

\begin{document}

\begin{center}
    \LARGE\doctitle\\
    \vspace{10pt}
    \small\supervisors\\
\end{center}
\hrule
\vspace{1pt}
\hrule height 1pt

\bigskip

% pointwise/pairwise/listwise approach?
% regression vs classification?
% second sentence waaaay too long
\begin{abstract}
In this thesis I want to explore the impact of pruning techniques for random
forests on distributed processing of learning-to-rank datasets. I will train a
forest ensemble on the LETOR-MQ2007 and/or MSLR-WEB10K~\cite{qin2013letor}
datasets using Apache Spark MLlib's~\cite{meng2015mllib} random forest
implementation and implement a pruning technique~\cite{nan2016pruning} on the
resulting model. I will explore the impact of the pruning step on scalability
and performance of the model through analysis of runtime and resource
consumption in comparison to the non-pruned model.

(79 words)
\end{abstract}

% START OF PROJECT DESCRIPTION -----------------------------------------------%
\section{Project description}
Random forests are an ensemble learning method for classification and/or
regression tasks (\cref{s:random_forests}). They correct for the decision
trees' habit of overfitting by combining many different trees and combining
their collective outcomes into a single output. The complexity of prediction
with these forests increases with the number of trees they contain. Especially
when the individual trees are very large, this can cause runtime and/or
resource issues. Pruning can be a very beneficial step to reduce the size of
the individual trees, cutting out parts of trees that do not have significant
impact in the prediction process. Decreasing the size of the individual trees
in a random forest will result in a smaller, quicker, potentially even more
accurate model. \\

I will be investigating the effect of pruning~\cite{nan2016pruning} on a random
forest classifier trained on the LETOR-MQ2007 and/or MSLR-WEB10K
datasets~\cite{qin2013letor} (for more info on LETOR, see \cref{s:letor}). To
test the effect I will be using Apache Spark (a distributed processing
framework). The random forest classifier is supplied by Apache Spark
MLlib~\cite{meng2015mllib}. Since Spark allows for multiple decision trees
predicting at the same time, it will be interesting to see if the pruning step
still has a significant impact on the runtime and resource consumption of the
random forest classifier.

% START OF BACKGROUND THEORY -------------------------------------------------%
\subsection{Random Forests}
\label{s:random_forests}
Random forests are an ensemble of decision trees. They combine many decision
trees to reduce the risk of overfitting common with using a single decision
tree.  All trees in the ensemble are trained separately, which allows for
parallel training.  Randomness is introduced in two ways:

\begin{enumerate}
    \setlength
    \item Training individual trees on subsets of the original dataset
        (bootstrapping).
    \item Using different random subsets of features to split the data on at
        each tree node.
\end{enumerate}

For classification problems, random forests come to a prediction by majority
vote. For regression problems, the outcomes of the individual trees are
averaged.

\subsection{Pruning}
Pruning tree-like models is a technique used to improve prediction performance.
It can result in higher prediction accuracy and/or meeting resource constraints
by decreasing the size of the model. The pruning technique that I will
implement~\cite{nan2016pruning} tries to optimize expected feature cost \&
accuracy. It does so by pruning random forests as a novel 0--1 integer program
with linear constraints. Combinatorial optimization exploits allow scaling to
large datasets, which is exactly what I am trying to achieve by using this
pruning technique in a distributed computing setting.

\subsection{Learning To Rank (LETOR)}
\label{s:letor}
LETOR tries to solve a ranking problem on a list of items. The aim is to find
an optimal ordering of the data based on their features. This problem is
different from traditional machine learning problems in the sense that it
does not necessarily care about the exact scores for individual data points,
where traditionally machine learning models come up with a predicted score for
single data points.

The two previously mentioned datasets consist of query-document pairs, each
labeled with a relevance value. The goal is to learn a model that can
accurately predict the relevance of an unseen query-document pair such that the
most relevant can appear first in search engine query results.

\subsubsection{Data}
Each row in the mentioned datasets~\cite{qin2013letor} is a query-document
pair, coupled with its relevance label (0--4). The label is stored in the first
column, the query in the second column, and the following columns of the row
are a feature vector representing a certain document. The last column contains
some comments about this particular query-document pair. Some example rows
taken from~\cite{qin2013letor}:

\begin{lstlisting}
2 qid:10032 1:0.056537 2:0.000000 ... 45:0.000000 46:0.076923 #docid = GX029-35-5894638 inc = 0.0119881192468859 prob = 0.139842
0 qid:10032 1:0.279152 2:0.000000 ... 45:0.250000 46:1.000000 #docid = GX030-77-6315042 inc = 1 prob = 0.341364
0 qid:10032 1:0.130742 2:0.000000 ... 45:0.750000 46:1.000000 #docid = GX140-98-13566007 inc = 1 prob = 0.0701303
1 qid:10032 1:0.593640 2:1.000000 ... 45:0.500000 46:0.000000 #docid = GX256-43-0740276 inc = 0.0136292023050293 prob = 0.400738
\end{lstlisting}

\subsubsection{Approaches}
\label{s:approaches}
There are three main approaches to the learning-to-rank problem~\cite{liu2009learning}:

\begin{enumerate}
    \item Pointwise approach: find the relevance degree of each single
        document.
    \item Pairwise approach: find the pairwise preference (i.e.\ A is more
        relevant than B) between each pair of documents.
    \item Listwise approach: find the preferred permutation of all documents,
        or find the relevance labels for all documents, depending on algorithm
        used.
\end{enumerate}

Since LETOR itself is not the focus of this thesis, I will be using the pointwise
approach since this is the most straightforward to use a decision tree-like
model with.

% START OF PLANNING ----------------------------------------------------------%
\section{Planning}

\begin{tabularx}{\textwidth}{ll|X}
Description & Deadline & Comment \\ \hline
Hand in final proposal & 04/11 & \\
Understand pruning technique & 11/10 & Understand well enough to be able to implement, also progress checkpoint this week \\
Train model on data + implement pruning & 18/11 & \\
Run performance tests and analyse results & 02/12 & Start writing thesis as well \\
Presentation & 21/12 & \\
Submit draft thesis & 23/12 & \\
Improve on thesis & 07/01 & \\
Defense & 20/01 & According to rules and regulations \\
\end{tabularx}

% START OF SCIENTIFIC RELEVANCE ----------------------------------------------%
\section{Relevance}
This thesis combines a relevant problem in todays age (LETOR) with a
distributed processing framework (Apache Spark) popular in the current big data
field. The pruning technique used in this thesis has not yet been used in such
an environment, so it is an interesting exploratory project.

\nocite{*}
\bibliographystyle{apacite}
\bibliography{main.bib}

\end{document}
