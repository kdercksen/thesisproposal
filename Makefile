DOCS := main

all: $(addsuffix .pdf, $(DOCS))

%.pdf: %.tex %.bib
	pdflatex $<
	bibtex $(basename $<)
	pdflatex $<
	pdflatex $<

clean:
	$(RM) -v $(addprefix $(DOCS).,log aux bbl blg)
